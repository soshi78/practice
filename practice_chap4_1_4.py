# -*- coding: utf-8 -*-
"""
Created on Sun Jul  2 11:59:30 2017
chap4.1.4
@author: syo
"""
class Michael:
    def __init__(self,max=5,count=0):
        self.__max = max
        self.__count= count
        
        #ゲッター
        def get_max(self):
            return self.__max

        def get_count(count):
            return self.__count
        
        #セッター
        def set_max(self, max):
            self.__max = max
            
        def set_count(self, count):
            self.__count = count
            
        #プロパティ定義
        max = property(get_max,set_max)
        count = property(get_count,set_count)
        
        def teach(self):
            if self.count < self.max:
                print("もっと強く")
            else:
                print("よーしオッケーだ")
            self.count += 1