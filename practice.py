# -*- coding: utf-8 -*-
"""
Created on Sun Jul  2 09:43:46 2017
practice
@author: syo
"""

#リスト内包表現
num_list = [num for num in range(1,6)]
print(num_list)

#num-1がリストの要素となる
num_list2 = [num-1 for num in range(1,6)]
print(num_list2)

#forループの中にif
num_list3 = [num for num in range(1,6) if num % 2 == 0]
print(num_list3)

#section4.1
class Michael:
    
    count = 0
    max = 5
    
#    def __init__(self,max):
#        self.max = max
#        self.count = 0

    @classmethod
    def teach(cls):
        if cls.count < cls.max:
            print("もっと強く")
        else:
            print("よーしオッケーだ")
        cls.count += 1

#oni = Michael(5)
#for i in range(6):
#    print("スマッシュ")
#    oni.teach()

for i in range(4):
    print("スマッシュ")
    Michael.teach()
    
for i in range(2):
    print("バックハンドストローク")
    Michael.teach()
